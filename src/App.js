import "./App.scss";
import AppHeader from "./components/AppHeader";
import { useState } from "react";
import { Route, Navigate, Routes, useNavigate } from "react-router-dom";
import CountriesList from "./components/CountriesList";
import CountryPage from "./components/CountryPage";

function App() {
  const [theme, setTheme] = useState("light");
  const [countries, setCountries] = useState("");
  const [selectedCountry, setSelectedCountry] = useState("");
  const navigate = useNavigate();

  function toCountryPageHandler(country) {
    setSelectedCountry(country);
    navigate(`/${country.altSpellings[0]}`);
  }

  return (
    <div className={`App ${theme}`}>
      <div className="App">
        <AppHeader theme={theme} setTheme={setTheme} />
        <main className="main">
          <Routes>
            <Route path="/" element={<Navigate to="/home" />} />
            <Route path="*" element={<Navigate to="/home" />} />
            <Route
              path="/home"
              element={
                <CountriesList
                  theme={theme}
                  countries={countries}
                  setCountries={setCountries}
                  selectedCountry={selectedCountry}
                  setSelectedCountry={toCountryPageHandler}
                />
              }
            />
            <Route
              path={selectedCountry && `/${selectedCountry.altSpellings[0]}`}
              element={
                <CountryPage
                  country={selectedCountry}
                  toBorderCountry={toCountryPageHandler}
                  countries={countries}
                />
              }
            />
          </Routes>
        </main>
      </div>
    </div>
  );
}

export default App;
