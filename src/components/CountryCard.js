import "./CountryCard.scss";

function CountryCard(props) {
  const c = props.country;

  function goToPageHandler() {
    props.setSelectedCountry(c);
  }

  return (
    <div className="country_parent" onClick={goToPageHandler}>
      <div className="icon_div">
        <img className="flag_icon" src={c.flags.png} alt=""></img>
      </div>
      <div className="country_data">
        <h4 className="counry_name">{c.name.official}</h4>
        <p>Population: {c.population.toLocaleString("en")}</p>
        <p>Region: {c.region}</p>
        <p>Capital: {c.capital}</p>
      </div>
    </div>
  );
}

export default CountryCard;
