import "./AppHeader.scss";
import DarkModeButton from "./DarkModeButton";

function Header(props) {
  return (
    <div className="header">
      <h2 className="header_text">Where in the world?</h2>
      <DarkModeButton theme={props.theme} setTheme={props.setTheme} />
    </div>
  );
}

export default Header;
