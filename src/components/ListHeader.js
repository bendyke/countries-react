import React from "react";
import "./ListHeader.scss";
import RegionFilter from "./RegionFilter";
import SearchBar from "./SearchBar";

function ListHeader(props) {
  const {
    countries,
    setFilteredCountries,
    setDisplayedList,
    updateListHandler,
  } = props;

  return (
    <div className="header_bar">
      <SearchBar
        theme={props.theme}
        countries={countries}
        setFilteredCountries={setFilteredCountries}
        setDisplayedList={setDisplayedList}
        updateListHandler={updateListHandler}
      />
      <RegionFilter
        className="region_filter"
        countries={countries}
        setFilteredCountries={setFilteredCountries}
        setDisplayedList={setDisplayedList}
        updateListHandler={updateListHandler}
      />
    </div>
  );
}

export default ListHeader;
