import React from "react";
import "./DarkModeButton.scss";
import darkmode_light from "../icons/dark-mode_light.png";
import darkmode_dark from "../icons/dark-mode_dark.png";
import { useEffect, useState } from "react";

function DarkModeButton(props) {
  const [icon, setIcon] = useState("");

  function switchDarkMode() {
    if (props.theme === "light") {
      props.setTheme("dark");
    } else {
      props.setTheme("light");
    }
  }
  useEffect(() => {
    if (props.theme === "light") {
      setIcon(darkmode_light);
    } else {
      setIcon(darkmode_dark);
    }
  }, [props.theme]);

  return (
    <>
      <button className="darkmode_button" onClick={switchDarkMode}>
        <img src={icon} className="icon" alt="" />
        <p className="darkmode_text"> Dark Mode</p>
      </button>
    </>
  );
}

export default DarkModeButton;
