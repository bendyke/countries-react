import React from "react";
import "./RegionFilter.scss";
import { useState } from "react";

function RegionFilter(props) {
  const [filterSelection, setFilterSelection] = useState("");
  const {
    countries,
    setFilteredCountries,
    setDisplayedList,
    updateListHandler,
  } = props;

  function changeHandler(event) {
    setFilterSelection(event.target.value);
    setDisplayedList([]);
    if (event.target.value === "All") {
      setFilteredCountries(countries);
    } else {
      setFilteredCountries(() => {
        let temp = countries.filter((country) => {
          return country.region === event.target.value;
        });
        return temp;
      });
    }
    updateListHandler();
  }

  return (
    <>
      <select
        className="region_filter"
        value={filterSelection}
        onChange={changeHandler}
        placeholder="Filter by region..."
      >
        <option value={"All"}>Select by Region</option>
        <option value={"Africa"}>Africa</option>
        <option value={"Americas"}>Americas</option>
        <option value={"Asia"}>Asia</option>
        <option value={"Europe"}>Europe</option>
        <option value={"Oceania"}>Oceania</option>
      </select>
    </>
  );
}

export default RegionFilter;
