import React from "react";
import "./BorderCountryButton.scss";

function BorderCountryButton(props) {
  function toBorderCountryHandler() {
    props.toBorderCountry(props.country);
  }

  return (
    <>
      <button className="border_button" onClick={toBorderCountryHandler}>
        {props.country.name.common}
      </button>
    </>
  );
}

export default BorderCountryButton;
