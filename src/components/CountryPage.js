import "./CountryPage.scss";
import React, { useEffect } from "react";
import { useNavigate } from "react-router";
import { useState } from "react/cjs/react.development";
import BorderCountryButton from "./BorderCountryButton";

function CountryPage(props) {
  const navigate = useNavigate();
  const country = props.country;
  const languageKeys = Object.keys(props.country.languages);
  const currencyKeys = Object.keys(country.currencies);
  const [nativeName, setNativeName] = useState([]);
  const [languages, setLanguages] = useState([]);
  const [borderCountries, setBorderCountries] = useState([]);

  function backToHomeHandler() {
    navigate("/home");
  }

  useEffect(() => {
    languageKeys.forEach((element) => {
      setLanguages([]);
      setNativeName([]);
      setBorderCountries([]);
      setNativeName((prevName) => {
        return [...prevName, country.name.nativeName[element].official];
      });
      setLanguages((prevLanguages) => {
        return [...prevLanguages, country.languages[element]];
      });
    });
    if (country.borders) {
      country.borders.forEach((element) => {
        setBorderCountries((prevBorderCountries) => {
          return [
            ...prevBorderCountries,
            props.countries.find((country) => country.cca3 === element),
          ];
        });
      });
      //scroll to top if we were re-directed
      window.scrollTo(0, 0);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [country]);

  return (
    <div className="page_parent">
      <div className="button_parent">
        <button className="button" onClick={backToHomeHandler}>
          Back
        </button>
      </div>
      <div className="content_parent">
        <img className="flag_img" src={country.flags.png} alt=""></img>
        <div className="content_wrapper">
          <div className="stats_wrapper">
            <div className="main_stats">
              <h2>{country.name.common}</h2>
              <h4>Native name: {nativeName.toString()}</h4>
              <h4>Population: {country.population.toLocaleString("en")}</h4>
              <h4>Region: {country.region}</h4>
              <h4>Sub Region: {country.subregion}</h4>
              <h4>Capital: {country.capital}</h4>
            </div>
            <div className="secondary_stats">
              <h4>Top Level Domain: {country.tld}</h4>
              <h4>Currencies: {currencyKeys.toString()}</h4>
              <h4>Langauges: {languages.toString()}</h4>
            </div>
          </div>
          <div className="border_buttons_parent">
            <h4>Border Countries: </h4>
            {borderCountries.map((e, index) => {
              return (
                <BorderCountryButton
                  className="border_button"
                  country={e}
                  toBorderCountry={props.toBorderCountry}
                  key={index}
                ></BorderCountryButton>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

export default CountryPage;
