import { useState, useEffect } from "react";
import axios from "axios";
import "./CountriesList.scss";
import CountryCard from "./CountryCard";
import InfiniteScroll from "react-infinite-scroll-component";
import ListHeader from "./ListHeader";

function CountriesList(props) {
  const [displayedList, setDisplayedList] = useState([]);
  const [updateDisplayedList, setUpdateDisplayedList] = useState(false);
  const [hasMore, setHasMore] = useState(true);
  const [filteredCountries, setFilteredCountries] = useState("");
  const { countries, setCountries } = props;

  useEffect(() => {
    if (countries === "") {
      axios.get("https://restcountries.com/v3.1/all").then(function (response) {
        // eslint-disable-next-line array-callback-return
        response.data.map((e) => {
          setCountries((prevCountries) => {
            return [...prevCountries, e];
          });
          setFilteredCountries((prevCountries) => {
            return [...prevCountries, e];
          });
        });
        setUpdateDisplayedList(true);
      });
    } else {
      setFilteredCountries(countries);
      setUpdateDisplayedList(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (updateDisplayedList) {
      for (let i = displayedList.length; i < displayedList.length + 8; i++) {
        if (!filteredCountries[i]) {
          setHasMore(false);
          setUpdateDisplayedList(false);
          return;
        }
        setHasMore(true);
        setDisplayedList((prevList) => {
          return [...prevList, filteredCountries[i]];
        });
      }
      setUpdateDisplayedList(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updateDisplayedList, filteredCountries]);

  function updateListHandler() {
    setUpdateDisplayedList(true);
  }

  return (
    <>
      <ListHeader
        theme={props.theme}
        countries={countries}
        setFilteredCountries={setFilteredCountries}
        setDisplayedList={setDisplayedList}
        updateListHandler={updateListHandler}
      />
      <InfiniteScroll
        className="scroll_component"
        dataLength={displayedList.length}
        next={updateListHandler}
        hasMore={hasMore}
        loader={<h2>Loading...</h2>}
        endMessage={<h2>No More Countries found</h2>}
      >
        <div className="countries_list">
          {displayedList.map((e, index) => {
            return (
              <CountryCard
                className="element"
                country={e}
                key={index}
                id={index}
                selectedCountry={props.selectedCountry}
                setSelectedCountry={props.setSelectedCountry}
              >
                {e.name.official}
              </CountryCard>
            );
          })}
        </div>
      </InfiniteScroll>
    </>
  );
}

export default CountriesList;
