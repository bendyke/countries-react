import React from "react";
import "./SearchBar.scss";
import { useState, useEffect } from "react";
import icon_dark from "../icons/search_icon_dark.png";
import icon_light from "../icons/search_icon_light.png";

function SearchBar(props) {
  const [icon, setIcon] = useState("");
  const [currentInput, setCurrentInput] = useState("");
  const {
    countries,
    setFilteredCountries,
    setDisplayedList,
    updateListHandler,
  } = props;

  function changeHandler(event) {
    setCurrentInput(event.target.value);
    setDisplayedList([]);
    if (event.target.value === "") {
      setFilteredCountries(countries);
    } else {
      setFilteredCountries(() => {
        let temp = countries.filter((country) => {
          return country.name.official
            .toLocaleLowerCase()
            .includes(event.target.value.toLocaleLowerCase());
        });
        return temp;
      });
    }
    updateListHandler();
  }

  useEffect(() => {
    if (props.theme === "light") {
      setIcon(icon_light);
    } else {
      setIcon(icon_dark);
    }
  }, [props.theme]);

  return (
    <div className="search_div">
      <img className="search_icon" src={icon} alt=""></img>
      <input
        className="search_bar"
        value={currentInput}
        placeholder="Search for a country..."
        onChange={changeHandler}
      ></input>
    </div>
  );
}

export default SearchBar;
